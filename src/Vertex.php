<?php

    namespace Graph;

    class Vertex
    {
        /**
         * @var int уникальное имя вершины
         */
        private $id;

        /**
         * @var array исходящие  ребра
         */
        private $edges = [];

        /**
         *  Двунаправленный коннект
         */
        const CONNECT_ALL = 0;
        /**
         * Конект к исходящей вершине
         */
        const CONNECT_OUT = 1;

        /**
         * Конект к приходящей вершине
         */
        const CONNECT_IN = 2;

        /**
         * Vertex constructor.
         * @param $id
         */
        public function __construct($id)
        {
            $this->id = $id;
            Graph::addVertex($this);
        }

        /**
         * Название вершины
         * @return mixed
         */
        public function getId()
        {
            return $this->id;
        }

        /**
         * Добавление ребра к вершине
         * @param Vertex $vertex Вершина к которой добавляется ребро
         */
        public function addEdgeTo(Vertex $vertex)
        {
            $this->edges[] = $vertex;
            Graph::addEdge($this, $vertex);
        }

        /**
         * Все вершины имеющие ребра с этой
         * @param Vertex $vertex
         * @return array
         */
        public static function getEdgesTo(Vertex $vertex)
        {
            /**
             * @var $outVertex Vertex
             */
            $res = [];

            return $res;
        }


        /**
         * Есть ли вершины исходящие от текущей
         * @return bool
         */
        public function isOutputEdges()
        {
            return !empty($this->edges);
        }

        /**
         * Исходит ли эта вершина от текущей
         * @param Vertex $vertex
         * @return bool
         */
        public function isOutputEdge(Vertex $vertex)
        {
            foreach ($this->getOutPutEdges() as $out) {
                if ($out->getId() == $vertex->getId()) {
                    return true;
                }
            }

            return false;
        }

        /**
         * Входит ли вершина в текущую
         * @param Vertex $vertex
         * @return bool
         */
        public function isIncomingEdge(Vertex $vertex)
        {
            /**
             * @var $out Vertex
             */
            foreach ($vertex->getOutPutEdges() as $out) {
                if ($out->getId() == $this->getId()) {
                    return true;
                }
            }

            return false;
        }

        /**
         * Есть ли входящие вершины в текущую
         * @return bool
         */
        public function isIncomingEdges()
        {
            $vertices = Graph::getVertices();

            foreach ($vertices as $outVertex) {
                /**
                 * @var $out Vertex
                 */
                foreach ($outVertex->getOutPutEdges() as $out) {
                    if ($out->getId() == $this->getId()) {
                        return true;
                    }
                }
            }

            return false;
        }

        /**
         * Исходящие вершины из текущей
         * @return array
         */
        public function getOutPutEdges()
        {
            return $this->edges;
        }

        /**
         * @return array
         */
        public function getIncomingEdges()
        {
            $ret = [];
            $getVertices = Graph::getVertices();
            foreach (Graph::getIn($this) as $id) {
                $ret[$id] = $getVertices[$id];
            }

            return array_values($ret);
        }

        /**
         * @param Vertex $expectedVertex
         * @return bool
         */
        public function isConnect(Vertex $expectedVertex)
        {
            $vertices = Graph::getVertices();
            foreach ($vertices as $vertex) {
                if ($vertex->isOutputEdge($expectedVertex)) {
                    return true;
                }

                if ($vertex->isIncomingEdge($expectedVertex)) {
                    return true;
                }
            }

            return false;
        }
    }

