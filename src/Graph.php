<?php

    namespace Graph;

    /**
     * Class Graph
     * @package Graph
     */
    class Graph
    {
        const TYPE_OUT = 0;
        const TYPE_IN = 1;

        /**
         * Все возможные вершины
         * @var Vertex[]
         */
        private static $vertices;

        private static $edges = [];

        /**
         * Добавить вершину в граф
         * @param Vertex $vertex
         */
        public static function addVertex(Vertex $vertex)
        {
            self::$vertices[$vertex->getId()] = $vertex;
        }

        /**
         * Все вершины графа
         * @return Vertex[]
         */
        public static function getVertices()
        {
            return self::$vertices;
        }

        public static function getEdges()
        {
            return self::$edges;
        }

        public static function addEdge(Vertex $vertexOut, Vertex $vertexIn)
        {
            // Исходящие
            $keyOut = $vertexOut->getId() . "-" . self::TYPE_OUT;
            if (empty($keyOut)) {
                self::$edges[$keyOut] = [];
            }
            self::$edges[$keyOut][] = $vertexIn->getId();

            // Входящие
            $keyIn = $vertexIn->getId() . "-" . self::TYPE_IN;
            if (empty(self::$edges[$keyIn])) {
                self::$edges[$keyIn] = [];
            }
            self::$edges[$keyIn][] = $vertexIn->getId();
        }

        public static function getOut(Vertex $vertex)
        {
            $keyOut = $vertex->getId() . "-" . self::TYPE_OUT;
            return self::$edges[$keyOut] ?? [];

        }


        public static function getIn(Vertex $vertex)
        {
            $keyIn = $vertex->getId() . "-" . self::TYPE_IN;

            return self::$edges[$keyIn] ?? [];
        }

        public static function getData(Vertex $vertex)
        {
            return [
                self::TYPE_IN => self::getIn($vertex),
                self::TYPE_OUT => self::getOut($vertex),
            ];
        }


        /**
         * количество вершин у графа
         * @return int
         */
        public static function count()
        {
            return count(self::$vertices);
        }
    }