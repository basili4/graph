<?php


    class EdgeToTest extends \PHPUnit\Framework\TestCase
    {

        public function testOutPuts()
        {
            $vertexId1 = new \Graph\Vertex('id1');
            $vertexId2 = new \Graph\Vertex('id2');
            $vertexId1->addEdgeTo($vertexId2);
            $this->assertTrue($vertexId1->isOutputEdges());
        }

        public function testIns()
        {
            $vertexId1 = new \Graph\Vertex('id1');
            $vertexId2 = new \Graph\Vertex('id2');

            $vertexId1->addEdgeTo($vertexId2);

            $this->assertTrue($vertexId2->isIncomingEdges());
        }


        public function testOutPut()
        {
            $vertexId1 = new \Graph\Vertex('id1');
            $vertexId2 = new \Graph\Vertex('id2');

            $vertexId1->addEdgeTo($vertexId2);

            $this->assertTrue($vertexId1->isOutputEdge($vertexId2));
        }


        public function testIn()
        {
            $vertexId1 = new \Graph\Vertex('id1');
            $vertexId2 = new \Graph\Vertex('id2');

            $vertexId1->addEdgeTo($vertexId2);

            $this->assertTrue($vertexId2->isIncomingEdges($vertexId1));
        }

        public function testGetInc()
        {
            $vertexId1 = new \Graph\Vertex('id1');
            $vertexId2 = new \Graph\Vertex('id2');
            $vertexId1->addEdgeTo($vertexId2);
            $incoming = $vertexId2->getIncomingEdges();
            $this->assertNotEmpty($incoming[0]);
            $this->assertEquals('id2', $incoming[0]->getId());
        }

        public function testGetOut()
        {
            $vertexId1 = new \Graph\Vertex('id1');
            $vertexId2 = new \Graph\Vertex('id2');
            $vertexId1->addEdgeTo($vertexId2);
            $outs = $vertexId1->getOutPutEdges();
            $this->assertNotEmpty($outs[0]);
            $this->assertEquals('id2', $outs[0]->getId());
        }

        public function testIsConnect()
        {
            $vertexId1 = new \Graph\Vertex('id1');
            $vertexId2 = new \Graph\Vertex('id2');
            $vertexId3 = new \Graph\Vertex('id3');
            $vertexId4 = new \Graph\Vertex('id4');

            $vertexId1->addEdgeTo($vertexId2);
            $vertexId2->addEdgeTo($vertexId3);

            $this->assertTrue($vertexId1->isConnect($vertexId3));
            $this->assertFalse($vertexId1->isConnect($vertexId4));
        }
    }
